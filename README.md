# README #

### Description ###

This is an empty Entando application with BPM plugin, that can be deployed to Openshift.


### Deploy to Openshift ###

Login into Openshift with oc cli, then execute oc commands:

* oc new-project "entando"
* oc new-app https://fuboleo@bitbucket.org/fuboleo/entando-bpm-demo.git --name=entando-bpm-demo
* oc expose svc entando-bpm-demo --name=entando-bpm-demo --path=/entando-bpm-demo/
