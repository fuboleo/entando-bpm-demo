FROM pmasala/entando-base-image-432

LABEL maintainer="Fu Bo Xia <f.xia@leonardo.com.au>"

ENV project entando-bpm-demo

COPY *.xml /opt/entando/${project}/
COPY src /opt/entando/${project}/src

USER root

RUN chmod -R 777 /opt/entando/${project}/

USER 1001

WORKDIR /opt/entando/${project}

ENTRYPOINT [ "mvn", "-Dmaven.repo.local=/opt/entando/.m2/repository", "jetty:run" ]

EXPOSE 8080